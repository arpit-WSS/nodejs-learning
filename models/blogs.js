import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const blogSchema = new Schema(
	{
		title: {
			type: String,
			required: true
		},
		snippets: {
			type: String,
			required: true
		},
		body: {
			type: String,
			required: true
		}
	},
	{ timestamps: true }
);

export const Blog = mongoose.model('Blog', blogSchema);
// <--- first argument will be Singular version of collection name.
//Mongoose will look for plural, lowercase version
