import { Blog } from '../models/blogs.js';

export const blog_index = (req, res) => {
	Blog.find()
		.sort({ createdAt: -1 })
		.then((result) => {
			res.render('index', { title: 'All Blogs', blogs: result });
		})
		.catch((err) => console.log(err));
};

export const blog_post = (req, res) => {
	const blog = new Blog(req.body);

	blog
		.save()
		.then((result) => {
			res.redirect('/blogs');
		})
		.catch((err) => console.log(err));
};

export const blog_details = (req, res) => {
	const id = req.params.id;
	Blog.findById(id)
		.then((results) => {
			res.render('details', { blog: results, title: 'Blog Details' });
		})
		.catch((err) => {
			res.status(404).render('404', { title: '404' });
		});
};

export const blog_delete = (req, res) => {
	const id = req.params.id;
	Blog.findByIdAndDelete(id)
		.then((result) => {
			res.json({ redirect: '/blogs' });
		})
		.catch((err) => console.log(err));
};

export const blog_create = (req, res) => {
	res.render('create', { title: 'create new blog' });
};
