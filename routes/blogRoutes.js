import express from 'express';
import { Blog } from '../models/blogs.js';
import { blog_create, blog_delete, blog_details, blog_index, blog_post } from '../controller/blogController.js';
const router = express.Router();

router.get('/create', blog_create);
router.get('/', blog_index);
router.post('/', blog_post);
router.get('/:id', blog_details);
router.delete('/:id', blog_delete);

export default router;
