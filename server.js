var http = require('http');
var fs = require('fs');

const server = http.createServer((req, res) => {
	console.log(req.url);

	//set content header
	res.setHeader('Content-Type', 'text/html');
	// res.write('<p>Hi GooD Morning</p>')

	//routing
	let path = './views/';
	switch (req.url) {
		case '/':
			path += 'index.html';
			res.statusCode = 200;
			break;
		case '/about':
			path += 'about.html';
			res.statusCode = 200;
			break;
		case '/about-me':
			res.statusCode = 301; // for redirect
			res.setHeader('Location', '/about');
			res.end();
		default:
			path += '404.html';
			res.statusCode = 404;
	}

	//send HTML file
	fs.readFile(path, (err, data) => {
		if (err) {
			console.log(err);
			res.end();
		} else {
			// res.write(data)
			res.end(data);
		}
	});
});

server.listen(3000, 'localhost', () => {
	console.log('listening for requests on localhost:3000');
});
