import express from 'express';
import morgan from 'morgan';
import mongoose from 'mongoose';
import { mdbURL } from './mdburl.js';
import blogRoutes from './routes/blogRoutes.js';
const app = express();

//connect DATABSE
mongoose
	.connect(mdbURL, { useNewUrlParser: true, useUnifiedTopology: true })
	.then((results) => app.listen(3000))
	.catch((error) => console.log(error));

// register view engine
app.set('view engine', 'ejs'); // <---- this will look files in views folder
// app.set('views' , 'myviews') // <---- this will look for files in myviews fodler

//third party middleware & //static files like style.css
app.use(morgan('tiny'));
app.use(express.static('public')); //<--- adding css link in head views/partials/head.ejs
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	// res.render('index');
	res.redirect('/blogs');
});

app.get('/about', (req, res) => {
	res.render('about', { title: 'About' });
});

//blog routes
app.use('/blogs', blogRoutes); //<--- second argument is applied to "/blogs" remove "/blogs" from blogRoutes

//404
app.use((req, res) => {
	res.status(404).render('404', { title: '404' });
});

// UNUSED

// app.get('/', (req, res) => {
// 	res.sendFile('./views/index.html', { root: __dirname });
// });

//middleware
// app.use((req, res, next) => {
// 	console.log('request made');
// 	console.log('host: ', req.hostname);
// 	console.log('path: ', req.path);

// 	next(); //<--- this moves middleware to next middleware
// });

// app.use((req, res, next) => {
// 	console.log('in the next middleware');
// });

// app.get('/about', (req, res) => {
// 	res.sendFile('./views/about.html', { root: __dirname });
// });

// //redirect
// app.get('/about-us', (req, res) => {
// 	res.redirect('/about');
// });

// app.get('/add-blog', (req, res) => {
// 	const blog = new Blog({
// 		title: 'new blog 2',
// 		snippets: 'About my new blog',
// 		body: 'Hahaha  this is a long lasting paragragh......... '
// 	});
// 	blog
// 		.save()
// 		.then((results) => {
// 			res.send(results);
// 		})
// 		.catch((err) => console.log(err));
// });

// app.get('/all-blogs', (req, res) => {
// 	Blog.find()
// 		.then((results) => {
// 			res.send(results);
// 		})
// 		.catch((err) => console.log(err));
// });

// app.get('/single-blog', (req, res) => {
// 	Blog.findById('62568cbaf51cfe6807c27afd')
// 		.then((results) => {
// 			res.send(results);
// 		})
// 		.catch((err) => console.log(err));
// });
